import { IsNotEmpty } from "class-validator";
import { CycleDto } from './cycle.dto'

export class EpicDto {
    @IsNotEmpty()
    title: string;
    @IsNotEmpty()
    description: string;
    @IsNotEmpty()
    tenantId: number
    @IsNotEmpty()
    cycle: CycleDto
}