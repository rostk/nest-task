import { IsNotEmpty } from "class-validator";
import { EpicDto } from "./epic.dto";

export class CycleDto {
    @IsNotEmpty()
    label: string;
    @IsNotEmpty()
    dateFrom: string;
    @IsNotEmpty()
    dateTo: string;
    @IsNotEmpty()
    tenantId: number;
    @IsNotEmpty()
    epic: EpicDto[];
}