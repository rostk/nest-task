import { Module } from '@nestjs/common';
import { EpicController } from '../controller';
import { EpicService } from '../service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EpicRepository } from '../repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([EpicRepository]),
  ],
  controllers: [EpicController],
  providers: [EpicService],
})
export class EpicModule {}
