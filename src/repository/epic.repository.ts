import { ConflictException } from '@nestjs/common';
import { Epic } from '../entity';
import { EntityRepository, Repository } from 'typeorm';


@EntityRepository(Epic)
export class EpicRepository extends Repository<Epic> {

    async createEpic(dto): Promise<Epic> {
        const { title, description, tenantId, cycle  } = dto;
        const epic = new Epic();
        epic.title = title;
        epic.description = description;
        epic.tenantId = tenantId;
        epic.cycle = cycle;
        try {
            await epic.save();
            return epic;
        } catch (e) {
            throw new ConflictException(e)
        }
    }

    async findAllEpics(): Promise<Epic[]>{
        return await Epic.find({
            relations: ['cycle'],
          });
    }

    async findEpicById(id): Promise<Epic[]>{
        return Epic.find({
            relations: ['cycle'],
            where: { id: { id } },
          });
    }
}


