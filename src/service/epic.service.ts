import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { EpicRepository } from '../repository';
import { Epic } from '../entity/epic.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { EpicDto } from '../dto/epic.dto';

@Injectable()
export class EpicService {

    constructor(
        @InjectRepository(EpicRepository)
        private epicRepository: EpicRepository,
    ) { }

    getAllEpic(): Promise<Epic[]> {
        return this.epicRepository.findAllEpics();
    }

    createEpic(dto: EpicDto): Promise<Epic> {
        return this.epicRepository.createEpic(dto);
    }

    async findEpicById(id: number): Promise<Epic> {
        const epic = await this.epicRepository.findOne(id);
        if (!epic) {
            throw new NotFoundException(`Epic with id ${id} not found`)
        }
        return epic;
    }

    async deleteEpicById(id: number): Promise<string> {
        await this.epicRepository.delete(id);
        return 'Epic deleted'
    }

    async updateEpic(id: number, dto: EpicDto): Promise<Epic> {
        const { title, description } = dto;
        const epic = await this.epicRepository.findOne(id);
        epic.title = title;
        epic.description = description;
        try {
            await epic.save();
            return epic;
        } catch (e) {
            throw new ConflictException(e)
        }
    }
}
