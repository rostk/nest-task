import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { EpicDto } from 'src/dto';
import { Epic } from '../entity';
import { EpicService } from '../service/epic.service';

@Controller()
export class EpicController {
    constructor(private epicService: EpicService) {}


    @Get()
    getAllTasks(): Promise<Epic[]> {
        return this.epicService.getAllEpic();
    }

    @Post()
    @UsePipes(ValidationPipe)
    createTask(@Body() dto: EpicDto): Promise<Epic>{
        return this.epicService.createEpic(dto);
    }

    @Get(':id')
    getTaksById(@Param('id', ParseIntPipe) id: number): Promise<Epic> {
        return this.epicService.findEpicById(id);
    }
    @Patch(':id')
    updateTask(@Param('id', ParseIntPipe) id: number, @Body() dto: EpicDto): Promise<Epic>{
        return this.epicService.updateEpic(id, dto);
    }

    @Delete(':id')
    deleteTaskById(@Param('id', ParseIntPipe) id: number): Promise<string> {
        return this.epicService.deleteEpicById(id);
    }
}
